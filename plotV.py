import numpy as np
import matplotlib.pyplot as plt

N = 0
tf = open(str(N)+'t', 'r')
bf = open(str(N)+'b', 'r')

xt = []
xb = []
yt = []
yb = []
vt = []
vb = []
ut = []
ub = []

for line in tf:
    pars = line.split()
    if len(pars) > 0:
        xt.append(float(pars[0]))
        yt.append(float(pars[1]))
        vt.append(2*float(pars[3]))
        ut.append(float(pars[2]))
for line in bf:
    pars = line.split()
    if len(pars) > 0:
        xb.append(float(pars[0]))
        yb.append(float(pars[1]))
        vb.append(2*float(pars[3]))
        ub.append(float(pars[2]))

f1 = plt.figure()
ax1 = f1.add_subplot(111)
ax1.plot(xt, vt, label='Top V')
ax1.plot(xb, vb, label='Bottom V')
ax1.plot(xt, ut, label='Top U')
ax1.plot(xb, ub, label='Bottom U')
ax1.set_title('U and V velocity vs chord length')
ax1.set_xlabel('chord length - x(m)')
ax1.set_ylabel('Velocity (m/s)')
ax1.legend()
#f2 = plt.figure()
#ax2 = f2.add_subplot(111)
#ax2.plot(xt, Lift)

plt.show()
