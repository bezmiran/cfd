"""
Potential flow assignment

Emma Smith
Shahzeb Imran

12/08/2017
"""

if __name__ == "__main__":
    print("Begin")

    #----------------------------------------------------------------------
    #Choose the total numberx_vo of points to use on the wing
    global t
    global c
    global fp
    global beta
    global theta
    global top_surface, bottom_surface, top_derive, bottom_derive, top_flap, bottom_flap, ftop, fbot

    N = 255#Number of vortices to use on the wing. Must be an odd number
    assert N % 2 == 1, "Number of points chosen was even, and should be odd"

    U = 20. #Freestream horizontal velocity (m/s)
    beta = radians(21.0) #Angle of attack
    V = U*tan(beta)#Freestream vertical velocity (m/s)

    c = 0.09 # chord length which is fixed by box size
    t = 0.1  # maximum thickness as fraction of chord length
    fp = 0.8 #flap hinge position as a fraction of chord length
    theta = -radians(20.0) #flap deflection angle in radians

    print("Doing analysis for NAXA00" + str(t*100))

    #----------------------------------------------------------------------
    #Choose the function that the top surface of the wing is
    def top_surface(x):
        return 5*t*c*(0.2969*sqrt(x/c)-0.1260*(x/c)-0.3516*(x/c)**2+0.2843*(x/c)**3-0.1015*(x/c)**4)

    def bottom_surface(x):
        return -(5*t*c*(0.2969*sqrt(x/c)-0.1260*(x/c)-0.3516*(x/c)**2+0.2843*(x/c)**3-0.1015*(x/c)**4))

    #Takes the fp value and deflects the coordinates to form a flap
    def top_flap(x):
        pivot_x = fp * c
        pivot_y = bottom_surface(pivot_x)

        y = top_surface(x)

        x -= pivot_x
        y -= pivot_y

        x_rotated = x * cos(theta) - y * sin(theta) + pivot_x
        y_rotated = x * sin(theta) + y * cos(theta) + pivot_y
        return x_rotated, y_rotated

    def bottom_flap(x):
        pivot_x = fp * c
        pivot_y = bottom_surface(pivot_x)

        y = bottom_surface(x)

        x -= pivot_x
        y -= pivot_y
        x_rotated = x * cos(theta) - y * sin(theta) + pivot_x
        y_rotated = x * sin(theta) + y * cos(theta) + pivot_y
        return x_rotated, y_rotated

    #=======================================================
    ###VORTICES
    #Need to create a list of x coordinates to place the vortices
    #the stagnation point will be placed at (0,0). Therefore:

    L_seg = c/((N-1)/2+1) #length of a segment = chord length / number of vortices on top

    #top and bottom x-vortex positions
    xtv = linspace(L_seg, c-L_seg, (N-1)/2)
    xbv = xtv.copy()

    #top and bottom y vortex position
    ytv = top_surface(xtv) #Finding the corresponding y values
    ybv = bottom_surface(xbv) #Finding the corresponding y values

    #deflect the vortices that come after the flap position to a new position
    for i in range(len(xtv)):
        if xtv[i] > fp * c:
           xtv[i], ytv[i] = top_flap(xtv[i])

    for i in range(len(xbv)):
        if xbv[i] > fp * c:
           xbv[i], ybv[i] = bottom_flap(xbv[i])

    #Doing the above leaves an ugly gap in the top surface, interpolate for top
    #and bottom surfaces and put vortices in evenly spaced positions
    ftop = CubicSpline(xtv, ytv)
    fbot = CubicSpline(xbv, ybv)

    xtv = linspace(L_seg, c-L_seg, (N-1)/2)
    xbv = xtv.copy()

    ytv = ftop(xtv)
    ybv = fbot(xbv)

    #creating a compiled list of all the vortices
    x_vortices = append(xtv, append(xbv, array([0])))
    y_vortices = append(ytv, append(ybv, array([0])))

    #Functions for derivative of top and bottom surfaces (after flap deflection)
    td = lambda x: derivative(ftop,x, dx = 1e-6)
    bd = lambda x: derivative(fbot,x, dx=1e-6)
    #-------------------------
    #CONSTRAINTS
    #each constraint will be half way between each of the vortices

    #x and y, top and bottom constraint positions based on the interpolated top
    #and bottom surfaces with the flap
    xtc = linspace(L_seg/2,c-1.5*L_seg,(N-1)/2)
    xbc = xtc.copy()

    ytc = ftop(xtc)
    ybc = fbot(xbc)

    #there needs to be a constraint on trailing edge, original coordinates are at (c, 0)
    #this code ensures that the trailing edge constraint is also rotated
    kutta_constraint_x, kutta_constraint_y = top_flap(c)
    kutta_constraint_x = array([kutta_constraint_x])
    kutta_constraint_y = array([kutta_constraint_y])

    #alpha represents the angle that the surface of the wing makes with the horizontal axis
    alpha_list = []
    for i in range(N-1):
        if i < (N-1)/2:
            alpha_list.append(arctan(td(xtc[i])))
        else:
            alpha_list.append(arctan(bd(xbc[i-(N-1)/2]))) #i-N-1/2 so is so that it points to the zero point in the list ie the first point in the bottom edge list.

    A = zeros((N,N)) # matrix of constraint equations
    bb = zeros(N) # right hadn side vector for constraint equations

    #i will be the row number (Aij would be ith row, jth column of A)

    for i in range((N-1)/2): #therefore creating the constraints for the top surface
        alpha = alpha_list[i]
        for j in range(N): # j will be the column number
            r2 = (xtc[i]-x_vortices[j])**2 + (ytc[i]-y_vortices[j])**2
            A[i][j] = -(ytc[i]-y_vortices[j])*sin(alpha)/2./pi/r2 - (xtc[i]-x_vortices[j])*cos(alpha)/2./pi/r2
        bb[i] = U*sin(alpha)-V*cos(alpha)

    for i in range((N-1)/2): #therefore creating the constraints for the bottom surface
        alpha = alpha_list[(N-1)/2 + i]
        for j in range(N):
            r2 = (xbc[i]-x_vortices[j])**2 + (ybc[i]-y_vortices[j])**2
            A[i+(N-1)/2][j] = -(ybc[i]-y_vortices[j])*sin(alpha)/2./pi/r2 - (xbc[i]-x_vortices[j])*cos(alpha)/2./pi/r2
        bb[i+(N-1)/2] = U*sin(alpha)-V*cos(alpha)


    #Weak kutta Condition
    A[-1][(N-1)/2 - 1] = 1.
    A[-1][-2] = 1.
    bb[-1] = 0
    vortex_strengths = linalg.solve(A,bb)

    #====================================================================
    #Plotting
    #plot airfoil shape
    fig = plt.figure()
    ax1 = fig.add_subplot(111)
    ax1.plot(x_vortices,y_vortices,'bx', label = 'Vortex locations')
    ax1.plot(xtc,ytc,'ro', label = 'Constraint locations')
    ax1.plot(xbc,ybc,'ro')
    ax1.plot(kutta_constraint_x, kutta_constraint_y, 'ro')
    mount1 = plt.Circle((0.02,0), 0.0025, fill=False)
    mount2 = plt.Circle((0.055,0), 0.0025, fill=False)
    ax1.add_artist(mount1)
    ax1.add_artist(mount2)
    ax1.axis([0,0.09, -0.008, 0.008])
    ax1.set_aspect('equal')

    fig2 = plt.figure()
    ax2 = fig2.add_subplot(111)
    print(sum(vortex_strengths))
    top_vortex_strengths = vortex_strengths[:(N-1)/2]
    bottom_vortex_strengths = vortex_strengths[(N-1)/2:-1]
    leading_vortex_strength = vortex_strengths[-1]
    ax2.plot(xtv, top_vortex_strengths, label='Upper Surface')
    ax2.plot(xbv, bottom_vortex_strengths, label='Lower Surface')
    ax2.plot([0], leading_vortex_strength)
    ax2.set_title('Vortex Strength vs chord length')
    ax2.set_xlabel('x(m)')
    ax2.set_ylabel('Vortex Strength')
    ax2.legend()

    #potential flow part
    mdata.name = 'Vortex + Uniform Flow adjacent to a wall'
    mdata.dimensions = 2

    UF = UniformFlow(U,V)

    Vortices = []
    for i in range(N):
        Vortices.append(Vortex(x_vortices[i],y_vortices[i],vortex_strengths[i]))

    visual.xmin=-0.5*c
    visual.xmax =1.5*c
    visual.ymin = -0.5*c
    visual.ymax = 0.5*c
    visual.subplot = 0

    shapex = concatenate([array([0]), xtv, xbv[::-1], array([0])])
    shapey = concatenate([array([0]), ytv, ybv[::-1], array([0])])
    plot.psi(levels = 50, shape=[shapex, shapey])
    plot.vectors_magU(min=0., max=20., shape=[shapex,shapey])
    plot.P(min=-270, max=0, shape=[shapex,shapey])
    plot.V(levels = 500, shape=[shapex,shapey])
    plot.U(min=0, max=25, shape=[shapex,shapey])
    plot.psi_magU(min=0, max=25, levels=50, shape=[shapex, shapey])

#mdata.name = 'Vortex + Uniform Flow adjacent to a wall'
#mdata.dimensions = 2

#a1=Source(0.5, 0.0,10.,label='Source')
#b=UniformFlow(5.,0.)

#visual.xmin=-2.
#visual.xmax =2.
#visual.subplot = 0

#plot.psi(levels = 20)
#plot.vectors_magU(min=0., max=20.)
#plot.P(min=-100, max=0)

screen.variables(['U', 'V'])
#x = screen.Lineval([0.0,0.0], [c,0.0], N=200)
points = []
for i in xrange(len(xtc)):
   a = [xtc[i], ytc[i]]
   points.append(a)
#for i in xrange(len(xbc)):
#   a = [xbc[i], ybc[i]]
#   points.append(a)
points.append([kutta_constraint_x[0], kutta_constraint_y[0]])
screen.locations(points)
