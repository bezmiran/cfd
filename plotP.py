import numpy as np
import matplotlib.pyplot as plt
import scipy.interpolate as interp
import scipy.misc as sp

angle =0 
alpha = np.radians(angle)
tf = open(str(angle)+'t', 'r')
bf = open(str(angle)+'b', 'r')

xt = []
xb = []
yt = []
yb = []
pt = []
pb = []

for line in tf:
    pars = line.split()
    if len(pars) > 0:
        xt.append(float(pars[0]))
        yt.append(float(pars[1]))
        pt.append(float(pars[4]))
for line in bf:
    pars = line.split()
    if len(pars) > 0:
        xb.append(float(pars[0]))
        yb.append(float(pars[1]))
        pb.append(float(pars[4]))
xt = np.array(xt)
xb = np.array(xb)
yt = np.array(yt)
yb = np.array(yb)
pt = np.array(pt)
pb = np.array(pb)

ftop = interp.CubicSpline(xt, yt)
fbot = interp.CubicSpline(xb, yb)

phit = -np.arctan(sp.derivative(ftop, xt, dx=1e-6))
phib = np.pi-np.arctan(sp.derivative(fbot, xb, dx=1e-6))

Ltop = np.trapz(-pt * np.cos(alpha + phit), x=xt)
Lbot = np.trapz(-pb * np.cos(alpha + phib), x=xb)
#print(Ltop, Lbot)
Ltot = Ltop + Lbot
print(Ltot)

Dtop = np.trapz(-pt * np.sin(alpha + phit), x=xt)
Dbot = np.trapz(-pb * np.sin(alpha + phib), x=xb)
#print(Dtop, Dbot)
Dtot = Dtop + Dbot
print(Dtot)

f1 = plt.figure()
ax2 = f1.add_subplot(111)
ax2.plot(xt, pt, label='Top Surface')
ax2.plot(xb, pb, label='Bottom Surface')
ax2.set_title('P vs chord length')
ax2.set_xlabel('x(m)')
ax2.set_ylabel('Pressure (Pa)')
ax2.legend()
plt.show()
