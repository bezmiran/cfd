import numpy as np

U = 2.0
r = np.sqrt(0.25**2.0 + 0.125**2.0)
points = [0, r, 2.*r]
theta = np.arctan(0.25/0.5)
A = np.ones([len(points),len(points)])
b = np.array([-U*np.sin(theta)]*len(points))

for i in range(len(points)):
    for j in range(len(points)):
        if i == j:
            A[i,j] = 0
        else:
            d = r*abs(i-j)
            A[i,j] = -(1./2.*np.pi) * ((points[i]-points[j])/d**2)
print(A)
print(b)
x = np.linalg.solve(A, b)
print(x)







